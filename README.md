# flyway-core-boomerang

Welcome to Flyway.
------------------
Database Migrations Made Easy.


Documentation
-------------
You can find getting started guides and reference documentation at https://flywaydb.org


Contributing
------------
Here is the info on how you can contribute in various ways to the project: https://flywaydb.org/documentation/contribute/


License
-------
Copyright (C) 2010-2015 Axel Fontaine

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.



Flyway is a registered trademark of Boxfuse GmbH.


############################ Modification Details Starts ############################

Below files are only Added or modified.

flyway-4.0.3/src/main/java/org/flywaydb/core/internal/dbsupport/snowflake/SnowflakeDBSupport.java

flyway-4.0.3/src/main/java/org/flywaydb/core/internal/dbsupport/snowflake/SnowflakeSchema.java

flyway-4.0.3/src/main/java/org/flywaydb/core/internal/dbsupport/snowflake/SnowflakeTable.java

flyway-4.0.3/src/main/java/org/flywaydb/core/internal/util/jdbc/DriverDataSource.java



############################ Modification Details End ############################


############################ Build Steps Starts ############################

# Use below command to build your jar
mvn -P-InstallableDBTest -P-CommercialDBTest clean install -DskipTests

# if not you would encounter below issues while building
# ---> Error #1

# [ERROR] Failed to execute goal on project flyway-core-boomerang: Could not resolve dependencies for project org.flywaydb:flyway-core-boomerang:jar:4.0.3-RELEASE: Failed to collect dependencies at com.microsoft.sqlserver:sqljdbc4:jar:4.0.2206.100: Failed to read artifact descriptor for com.microsoft.sqlserver:sqljdbc4:jar:4.0.2206.100: Could not transfer artifact com.microsoft.sqlserver:sqljdbc4:pom:4.0.2206.100 from/to flyway-repo (s3://flyway-repo/release): The S3 wagon needs AWS Access Key set as the username and AWS Secret Key set as the password. eg:
[ERROR] <server>
[ERROR] <id>my.server</id>
[ERROR] <username>[AWS Access Key ID]</username>
[ERROR] <password>[AWS Secret Access Key]</password>
[ERROR] </server>

############################ Build Steps Ends ############################